const addEventListner = kind => {
  const array = document.querySelectorAll(`#${kind}`);
  for (let i = 0; i < array.length; i++) {
    array[i].addEventListener("click", () => {
      removeWork(
        array[i].parentElement.firstElementChild.innerHTML + "-" + kind,
        kind
      );
    });
  }
};

const removeEventListener = kind => {
  const array = document.querySelectorAll(`#${kind}`);
  for (let i = 0; i < array.length; i++) {
    array[i].removeEventListener();
  }
};

const updateNumber = kind => {
  const array = document.querySelectorAll(`#${kind}`);
  for (let i = 0; i < array.length; i++) {
    array[i].parentElement.firstElementChild.innerHTML = i + 1;
    array[i].parentElement.firstElementChild.id = `${i + 1}-${kind}`;
  }
};

const removeWork = (id, kind) => {
  document.getElementById(id).parentElement.remove();
  updateNumber(kind);
  removeEventListener(kind);
  addEventListner(kind);
};

const addWork = kind => {
  const length = document.querySelectorAll(`#${kind}`).length;
  const input = document.getElementById(`input-${kind}`).value;
  if (input !== "") {
    document.getElementById(`input-${kind}`).value = "";
    const work = `<div class="work"><em id="${length + 1}-${kind}">${length +
      1}</em><div class="text">${input}</div><button id="${kind}" class="remove">Remove</button></div>`;
    document.querySelector(`#${kind}-list`).innerHTML += work;
    addEventListner(kind);
  }
};

// Event listener for adding work
document
  .querySelector("#add-todo")
  .addEventListener("click", () => addWork("todo"));
document
  .querySelector("#add-doing")
  .addEventListener("click", () => addWork("doing"));
document
  .querySelector("#add-done")
  .addEventListener("click", () => addWork("done"));
